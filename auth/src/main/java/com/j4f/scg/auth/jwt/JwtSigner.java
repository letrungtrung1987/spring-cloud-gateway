package com.j4f.scg.auth.jwt;

import java.util.Collection;

public interface JwtSigner {
    default String createJwt(String userId) {
        return createJwt(userId, null);
    }

    String createJwt(String userId, Collection<String> roles);

    JwtUser verifyJwt(String jwt);
}
