package com.j4f.scg.service.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class GreetingController {
    private static final String X_JWT_SUB_HEADER = "X-jwt-sub";

    @GetMapping("greeting")
    public Mono<String> greeting(@RequestHeader(value = X_JWT_SUB_HEADER, defaultValue = "World") String name) {
        return Mono.just(String.format("Hello %s!", name));
    }

}
