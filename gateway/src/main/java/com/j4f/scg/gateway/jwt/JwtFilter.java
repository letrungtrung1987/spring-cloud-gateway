package com.j4f.scg.gateway.jwt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Component
public class JwtFilter extends AbstractNameValueGatewayFilterFactory {

    private static final String WWW_AUTH_HEADER = "WWW-Authenticate";
    private static final String X_JWT_SUB_HEADER = "X-jwt-sub";


    @Autowired
    private JwtAuthService jwtService;


    @Override
    public GatewayFilter apply(NameValueConfig config) {
        return (exchange, chain) -> {

            try {
                String token = this.extractJWTToken(exchange.getRequest());

                return jwtService.verify(token)
                        .flatMap(jwtUser -> {
                            ServerHttpRequest request = exchange.getRequest().mutate()
                                    .header(X_JWT_SUB_HEADER, jwtUser.getUserId())
                                    .build();

                            return chain.filter(exchange.mutate().request(request).build());
                        })
                        .doOnError(throwable -> {
                            System.out.println(throwable);
                        })
                        .switchIfEmpty(Mono.defer(() -> this.onError(exchange, "Jwt invalid")));
            } catch (Throwable ex) {

                log.error(ex.toString());
                return this.onError(exchange, ex.getMessage());
            }
        };
    }

    private Mono<Void> onError(ServerWebExchange exchange, String err) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        response.getHeaders().add(WWW_AUTH_HEADER, this.formatErrorMsg(err));

        return response.setComplete();
    }

    private String extractJWTToken(ServerHttpRequest request) {
        if (!request.getHeaders().containsKey("Authorization")) {
            throw new JwtTokenException("Authorization header is missing");
        }

        List<String> headers = request.getHeaders().get("Authorization");
        if (headers.isEmpty()) {
            throw new JwtTokenException("Authorization header is empty");
        }

        String credential = headers.get(0).trim();
        String[] components = credential.split("\\s");

        if (components.length != 2) {
            throw new JwtTokenException("Malformat Authorization content");
        }

        if (!components[0].equals("Bearer")) {
            throw new JwtTokenException("Bearer is needed");
        }

        return components[1].trim();
    }

    private String formatErrorMsg(String msg) {
        return String.format("Bearer realm=\"acm.com\", " +
                "error=\"https://tools.ietf.org/html/rfc7519\", " +
                "error_description=\"%s\" ", msg);
    }
}
