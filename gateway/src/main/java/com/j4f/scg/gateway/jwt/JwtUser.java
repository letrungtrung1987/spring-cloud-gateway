package com.j4f.scg.gateway.jwt;

import lombok.Data;

import java.util.Collection;
import java.util.Date;

@Data
public class JwtUser {
    String userId;

    Collection<String> roles;

    Date expiryDate;
}
