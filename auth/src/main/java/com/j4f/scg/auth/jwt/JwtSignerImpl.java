package com.j4f.scg.auth.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.InvalidKeyException;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;

@Component
public class JwtSignerImpl implements JwtSigner {
    final JwtProperties properties;

    final SecretKey secretKey;

    final JwtParser jwtParser;

    public JwtSignerImpl(JwtProperties properties) {
        String secret = properties.getSecret();
        if (secret == null)
            throw new InvalidKeyException("JWT is empty");

        this.secretKey = Keys.hmacShaKeyFor(secret.getBytes());
        this.jwtParser = Jwts.parserBuilder()
                .setSigningKey(secretKey)
                .requireIssuer(properties.getIssuer())
                .build();
        this.properties = properties;
    }


    @Override
    public String createJwt(String userId, Collection<String> roles) {
        return Jwts.builder()
                .signWith(secretKey)
                .setSubject(userId)
                .setIssuer(properties.getIssuer())
//                .claim("role", roles)
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(properties.getExpiry())))
                .setIssuedAt(Date.from(Instant.now()))
                .compact();
    }


    @Override
    public JwtUser verifyJwt(String jwt) {
        Claims body = jwtParser.parseClaimsJws(jwt).getBody();
        return new JwtUser()
                .setExpiryDate(body.getExpiration())
                .setUserId(body.getSubject());
    }

}
