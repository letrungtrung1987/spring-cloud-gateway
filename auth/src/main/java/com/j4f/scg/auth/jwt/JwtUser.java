package com.j4f.scg.auth.jwt;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Collection;
import java.util.Date;

@Data
@Accessors(chain = true)
public class JwtUser {
    String userId;

    Collection<String> roles;

    Date expiryDate;
}
