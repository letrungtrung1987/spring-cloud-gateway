package com.j4f.scg.auth.controller;

import com.j4f.scg.auth.jwt.JwtSigner;
import com.j4f.scg.auth.jwt.JwtUser;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RestController
public class JwtAuthController {
    static final String BEARER_PREFIX = "Bearer ";

    final JwtSigner jwtSigner;

    @PostMapping("jwt/verify")
    public Mono<JwtUser> verify(@RequestHeader(value = HttpHeaders.AUTHORIZATION, required = false) String token) {
        return Mono.justOrEmpty(token)
                .filter(s -> s.startsWith(BEARER_PREFIX))
                .map(s -> s.substring(BEARER_PREFIX.length()).trim())
                .map(jwtSigner::verifyJwt)
                .switchIfEmpty(Mono.empty());
    }

}
