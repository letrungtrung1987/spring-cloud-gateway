package com.j4f.scg.auth.jwt;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Duration;


@Data @Component
@FieldDefaults(level = AccessLevel.PRIVATE)
@ConfigurationProperties("jwt")
public class JwtProperties {
    String secret;
    String issuer;
    Duration expiry = Duration.ofMinutes(30);
}
