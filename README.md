# Hiding Services & Runtime Discovery with Spring Cloud Gateway

Thanks for [Ben Wilcock][30] 
[Brian McClain][31] – Technical Marketing, Pivotal.

It's rare for a company to want _every_ API to be publicly accessible. Most prefer to keep their services secret by default, only exposing APIs publicly when absolutely necessary. 

[Spring Cloud Gateway][14] can help. Spring Cloud Gateway allows you to route traffic to your APIs using simple Java™ instructions (which we saw [in the last article][15]) or with YAML configuration files (which we’ll demonstrate in this one). To hide your services, you set up your network so that the only server accessible from the outside is the gateway. The gateway then becomes a gate-keeper, controlling ingress and egress from outside. It’s a very popular pattern.

Cloud-based services also have a habit of changing location and granularity without much warning. To cope better with this, you can combine a gateway with a service registry to allow the applications on your network to find each other dynamically at runtime. If you do this, your applications will be much more resilient to changes. [Spring Cloud Netflix Eureka Server][13] is one such service registry.

In this post, we'll look at Spring Cloud’s gateway and registry components and illustrate how you can use them together to make your applications more secure and more reliable. 

Because this arrangement requires a particular setup, we’ve provided ready-to-run code which you can download and run. We’ll be using Docker to orchestrate our services and emulate a private network. We’ll then talk to our running services using HTTPie.

## Let’s Get Started...

The Gateway's configured URL passthrough paths can be seen in the Gateway's [application.yml file][9]. This configuration is using the "logical" names of these servers and the `lb:` (load balancer) protocol as shown in the extract below:

```yaml
spring:
  application:
    name: gateway  
  cloud:
    gateway:
      routes:
      - id: service
        uri: lb://service
        predicates:
        - Path=/service/**
        filters:
        - StripPrefix=1
...
```

By using these 'logical' server names, the Gateway can use the Registry to discover the true location of these services at runtime.

## Wrapping Up

With the entire Spring toolkit at your disposal, it quickly becomes apparent how flexible and powerful Spring Cloud Gateway can be. If you dig into [the source code][3], you’ll notice that with just a few lines of Java and a few dependencies, we can easily integrate Spring Boot microservices with Eureka, and control access to our service’s APIs. 

Before you finish, why not sign up for [SpringOne Platform 2019][18] – the premier conference for building scalable applications that delight users. Join thousands of like-minded Spring developers to learn, share, and have fun in Austin, TX from October 7th to 10th.  Use the discount code **S1P_Save200** when registering to save money on your ticket. If you need help convincing your manager use [this page][19]. See you there.

[1]: https://www.docker.com/products/docker-desktop
[2]: https://buildpacks.io/docs/app-journey/
[3]: https://bitbucket.org/letrungtrung1987/spring-cloud-gateway
[4]: https://spring.io/guides/gs/rest-service/
[5]: https://bitbucket.org/letrungtrung1987/spring-cloud-gateway/src/master/gateway
[6]: https://bitbucket.org/letrungtrung1987/spring-cloud-gateway/src/master/registry
[7]: https://bitbucket.org/letrungtrung1987/spring-cloud-gateway/src/master/service
[8]: https://github.com/benwilcock/spring-cloud-gateway-demo/blob/master/runtime-discovery/docker-compose.yml
[9]: https://bitbucket.org/letrungtrung1987/spring-cloud-gateway/src/master/gateway/src/main/resources/application.yml
[10]: http://localhost:8080/registry
[11]: http://localhost:8080/service/greeting

[13]: https://spring.io/guides/gs/service-registration-and-discovery/
[14]: https://spring.io/guides/gs/gateway/
[15]: https://content.pivotal.io/practitioners/getting-started-with-spring-cloud-gateway-3
[16]: https://httpie.org/
[17]: https://github.com/OlgaMaciaszek/spring-cloud-netflix-demo
[18]: https://springoneplatform.io/
[19]: https://springoneplatform.io/2019/convince-your-manager
[20]: https://docs.docker.com/compose/
[21]: https://github.com/benwilcock/spring-cloud-gateway-demo/blob/master/runtime-discovery/pack-images.yml


[unreachable]: https://static.spring.io/blog/bwilcock/20190701/unreachable.png "Screenshot from the browser window showing that the service is unreachable"
[registry]: https://static.spring.io/blog/bwilcock/20190701/registry.png "Screenshot showing the Eureka registry console in a browser"

[30]: https://twitter.com/benbravo73
[31]: https://twitter.com/BrianMMcClain