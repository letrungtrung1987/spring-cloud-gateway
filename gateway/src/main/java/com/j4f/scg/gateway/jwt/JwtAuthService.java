package com.j4f.scg.gateway.jwt;

import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import reactivefeign.spring.config.ReactiveFeignClient;
import reactor.core.publisher.Mono;

@ReactiveFeignClient("${auth.service}")
public interface JwtAuthService {

    @PostMapping("${auth.jwt-verify-path}")
    Mono<JwtUser> verify(@RequestHeader(HttpHeaders.AUTHORIZATION) String token);

}
